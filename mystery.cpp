#include <iostream>
#include <vector>
using namespace std;

void print(auto A)
{
   for (auto a : A) 
        cout <<a<<" ";

   cout<<endl;
}

void mystery1(auto& Data)
{
  cout<<endl<<"Mystery 1"<<endl<<"---------------------"<<endl;

  for ( int i = 0 ; i < Data.size( ) ; i++)
  {
    for ( int j = 0 ; j < i ; j++)
	if ( Data[ i ] < Data[ j ] )
	    swap( Data[ i ] , Data[ j ] );

    print(Data);
  }//end outer for (this brace is needed to include the print statement)

}


//Selection sort
void mystery2(auto& Data)
{
	int c, b, minIndex, temp;
	
	cout<<endl<<"Mystery 2"<<endl<<"---------------------"<<endl;
	
	for (int c = 0; c < Data.size()-1; c++)
	{
		minIndex = c;
		
		for(int b = c + 1; b < Data.size(); b++)
		{
			if (Data[b] < Data[minIndex])
			{
				minIndex = b;
			}
			if (minIndex != c)
			{
				temp = Data[c];
				Data[c] = Data[minIndex];
				Data[minIndex] = temp;
			}
			
		}
		print(Data);
	}
}


//Insertion sort
void mystery3(auto& Data)
{
		int nextIndex, moveItem, insertVal;
	
		cout<<endl<<"Mystery 2"<<endl<<"---------------------"<<endl;
		
		for(nextIndex = 1; nextIndex < Data.size(); nextIndex++)
		{
			insertVal = Data[nextIndex];
			moveItem = nextIndex;
			
			while (moveItem > 0 && Data[moveItem -1] > insertVal)
			{
				Data[moveItem] = Data[moveItem -1];
				moveItem--;
			}
			
			Data[moveItem] = insertVal;
			print(Data);
		}
		
	
}

//... Other mysteries...

int main()
{
    
  vector<int> Data = {36, 18, 22, 30, 29, 25, 12};

  vector<int> D1 = Data;
  vector<int> D2 = Data;
  vector<int> D3 = Data;

  mystery1(D1);
  mystery2(D2);
  mystery3(D3);

}
